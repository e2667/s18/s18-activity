function Pokemon(name, level){
	//properties
	this.name = name;
	this.level = level;
	this.health = 3 * level;
	this.attack = level;
	//methods
	this.tackle = function(target){
		
		console.log(this.name + ' tackled ' + target.name);
		target.health = target.health - this.level
		console.log(target.name + "'s health is now reduced to " + (target.health - this.attack));
		if(target.health < 5){
			target.faint()
		}

	},
	this.faint = function(){
		// if(this.health >= 5){
			console.log(this.name + " fainted.")
		}
	}

 //Creates a new instance of the "Pokemon" object each with their unique properties
let pikachu = new Pokemon("Pikachu", 16);
let squirtle = new Pokemon("Squirtle", 8);


pikachu.tackle(squirtle)
pikachu.tackle(squirtle)

let onix = new Pokemon("onix", 10);
pikachu.tackle(onix)
pikachu.tackle(onix)
